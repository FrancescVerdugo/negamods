@extends('layouts.app')

@section('content')

    <div class="container" style="width: 70%">
        <br/>
        <form action="/updateGuia/{{ $guia[0] -> id }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="Foto">Foto:</label>
                <input class="" type="file" id="foto" name="foto"  accept="image/*"  value="{{$guia[0] -> idFoto}}">
            </div>
            <div class="form-group">

                <label for="titol">Titulo:</label>
                <input type="text" class="form-control" id="titol" name="titol"  value="{{$guia[0] -> titol}}" maxlength="47" required/>

            </div>

            <div class="form-group">
                <label for="descripcio">Descripción:</label>
                <textarea class="form-control" id="descripcio" name="descripcio" required maxlength="1000">{{$guia[0] -> descripcio}} required</textarea>
            </div>

            <div class="">
                <input class="" type="file" id="archivo" name="archivo" accept=".pdf" value="{{$guia[0] -> idFitxer}}" required>
            </div>
            <br/>
            <div class="d-flex">
                <div class="button pr-2">
                    <button type="submit" class="btn btn-primary">Subir</button>
                </div>
                <div class="pl-2">
                    <form action="/miContenido/" method="get">
                        @csrf
                        <button type="submit" class="btn btn-danger">Cancelar</button>
                    </form>
                </div>
            </div>
        </form>
    </div>
@endsection