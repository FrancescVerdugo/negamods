@extends('layouts.app')

@section('css-secundario')
    <link href="{{ asset('css/comentaris.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="container">

        <h2>Información detallada</h2>
        <hr/>
        <div><h3>{{$guies->titol}}</h3></div>
        <div class="d-flex flex-row ">
            <div class="p-0 col-3 pb-3"><img class="img-fluid border-dark" src="{{ asset($foto->ruta. $foto->nom) }}"></div>
            <div class="p-0 col-9 pl-2 pb-3">

                <p><span class="badge badge-secondary">Fecha: {{$guies->created_at}}</span></p>
                <p><span class="badge badge-secondary">Autor:

                    @foreach($user as $users)
                            @if($users->id == $guies->idUser)
                                {{$users->name}}
                            @endif
                        @endforeach
                    </span>
                </p>
            </div>
        </div>

        <div class="p-2 border" style="background-color: #EEEFF3">
            <p>
                <strong>Descripción:</strong><br/>
                {{$guies->descripcio}}
            </p>
        </div>
        <div class="d-flex flex-row">
            <p class="p-2">Visitas: {{$guies->visites}}</p>
            <p class="p-2">Descargas: {{$guies->descargues}}</p>
            <p class="p-2">
                @guest
                    <a href="{{ route('login') }}">
                        <button type="button" class="btn btn-success">Descargar</button>
                    </a>
                @else
                    <a href="/downloadGuia/{{$guies -> id}}/{{$guies -> idFitxer}}">
                        <button type="button" class="btn btn-success">Descargar</button>
                    </a>
                @endguest
            </p>
        </div>

        <div>
        <iframe id="iframepdf" src="{{ asset($fitxer->ruta. $fitxer->nom) }}?page=hsn#toolbar=0" style="height: 700px; width:100%;"></iframe>
        </div>
        <hr/>
        @guest
        @else
            <div class="form-group">
                <form method="POST" action="/guardarComentari">
                    @csrf
                    <div class="input_comment">
                        <input required name="comentariNou" class="form-control"id="comentariNou" type="text" placeholder="Añadir comentario como {{ Auth::user()->name }}...">
                    </div>
                    <input type="hidden" value="{{ $guies->id }}" name="idGuia" id="idGuia">
                    <input type="hidden" value="esGuia" name="esGuia" id="esGuia">
                    <div class="">
                        <button type="submit" class="btn btn-primary">
                            Enviar
                        </button>
                    </div>
                </form>
            </div>


        @endguest

        <div class="footer" >
            <p>Comentarios:</p>

            @foreach($comentaris as $comentari)
                <div class="container border">
                    <div class="d-flex row" style="background-color: #D0D2D8">

                        @foreach($user as $users)
                            @if($users->id == $comentari->idUser)
                                <div class="col-11">&nbsp&nbsp<strong> {{$users->name}}</strong>&nbsp&nbsp&nbsp&nbsp{{$comentari->created_at}}</div>
                                @guest
                                @else
                                    @if( Auth::user()->id  == $comentari->idUser)
                                        <div class="col-1 text-right pr-0">
                                            <form action="/deleteComentariGuia/{{$comentari -> id}}/{{$guies->id}}" method="get">
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-sm">X</button>
                                            </form>
                                        </div>
                                    @endif
                                @endguest
                            @endif
                        @endforeach

                    </div>
                    <div>
                        <p>&nbsp&nbsp{{$comentari->contingut}}</p>
                    </div>
                </div>
                <br/>
            @endforeach

        </div>
    </div>
@endsection