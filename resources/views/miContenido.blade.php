@extends('layouts.app')

@section('content')
    <?php $count1=0;?>
    <?php $count2=0;?>

    <div class="container">
        <div class="text-center"><h1>Mi Contendo</h1></div>
        <br/>
        <div class="row justify-content-between">

        <div class="col-6 pr-4">
            <h1>Mods:</h1>


        @foreach($videojoc as $videojocs)
           <?php $numMod=0; ?>

            @foreach($mods as $mod)
                @if($videojocs->id == $mod->idVideojoc)
                       <?php ++$numMod; ?>
                            @if($numMod==1)
                               <hr/>
                               <div class="row  p-0 border " style="background-color: #EEEFF3">

                                   <div class="col-4 p-2 text-center">
                                       @foreach($fotos as $foto)
                                           @if($videojocs->idFoto == $foto->id)
                                               <img class="border-dark img-fluid" style="width: 80%"  src="{{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}">
                                           @endif
                                       @endforeach
                                   </div>

                                   <div class="col-8 p-0 ">
                                       <div class="row">
                                           <div class="col-10">
                                               <h3>{{$videojocs->nom}} - Mods</h3>
                                           </div>
                                       </div>
                                       <div>
                                           <p class="badge badge-secondary">{{ $videojocs -> data }}</p>
                                       </div>
                                   </div>


                               </div>
                                <br/>
                           @endif
        <div class="flex-row">
            <div class="p-2">
                <button class="navbar-toggler pl-3 pr-3 pt-1 pb-1 bg-info text-white" style="width: 100%" type="button" data-toggle="collapse" data-target="#collapseA<?php echo $count1?>" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

                    <div class="row ">
                        <div class="col-7" style="text-align: left">{{ $mod -> titol }}</div>
                        <div class="col-5 d-flex">
                            <div class="col-6 d-flex">
                                <p class="d-flex"><img src="{{ asset('img/icons/eye-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $mod -> visites }}</p>
                            </div>
                            <div class="col-6 d-flex flex-row pl-4">
                                <p class="d-flex"><img src="{{ asset('img/icons/data-transfer-download-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $mod -> descargues }}</p>
                            </div>
                        </div>
                    </div>

                </button>
                <div class="collapse" style="background-color: #F0F0F0;"id="collapseA<?php echo $count1?>">
                    <div class="navbar-nav ml-auto">
                        <div class="cosa">

                            <div class="d-flex flex-row">
                                @foreach($fotos as $foto)
                                    @if($mod->idFoto == $foto->id)
                                        <div class="p-2 col-4"><img class="img-fluid border-dark" src="{{ asset($foto->ruta. $foto->nom) }}">
                                        </div>
                                    @endif
                                @endforeach
                                <div class="p-2 col-8">

                                    <p class="badge badge-secondary">Fecha: {{$mod->created_at}}</p>

                                    <p><strong>Descripción: </strong>{{$mod->descripcio}}</p>
                                </div>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="p-2">
                                    <form action="/mods/{{$mod -> id}}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-primary">Ver mas</button>
                                    </form>
                                </div>
                                <div class="p-2 pr-5">
                                    @foreach($fitxer as $fitxers)
                                        @if($fitxers->id == $mod->idFitxer)
                                            @guest
                                                <a href="{{ route('login') }}">
                                                    <button type="button" class="btn btn-success">Descargar</button>
                                                </a>

                                            @else
                                                <a href="/downloadMod/{{$mod -> id}}/{{$mod -> idFitxer}}">
                                                    <button type="button" class="btn btn-success">Descargar</button>
                                                </a>
                                            @endguest
                                        @endif
                                    @endforeach
                                </div>
                                <div class="p-2 ">
                                    <form action="/editMod/{{$mod -> id}}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-warning">Editar</button>
                                    </form>
                                </div>
                                <div class="p-1 pt-2 ">
                                    <form action="/deleteMod/{{$mod -> id}}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
                @endif
                    <?php ++$count1?>
            @endforeach
        @endforeach

        </div>

    <div class="col-6 pl-4">
        <h1>Guies:</h1>

        @foreach($videojoc as $videojocs)
            <?php $numGuia=0; ?>

            @foreach($guies as $guia)
                @if($videojocs->id == $guia->idVideojoc)
                    <?php ++$numGuia; ?>
                    @if($numGuia==1)
                            <hr/>
                            <div class="row  p-0  border" style="background-color: #EEEFF3">

                                <div class="col-4 p-2 text-center">
                                    @foreach($fotos as $foto)
                                        @if($videojocs->idFoto == $foto->id)
                                            <img class="border-dark img-fluid" style="width: 80%"  src="{{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}">
                                        @endif
                                    @endforeach
                                </div>

                                <div class="col-8 p-0 ">
                                    <div class="row">
                                        <div class="col-10">
                                            <h3>{{$videojocs->nom}} - Mods</h3>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="badge badge-secondary">{{ $videojocs -> data }}</p>
                                    </div>
                                </div>


                            </div>
                            <br/>
                    @endif
                    <div class="flex-row ">
                        <div class="p-2">
                            <button class="navbar-toggler pl-3 pr-3 pt-1 pb-1 bg-info text-white" style="width: 100%" type="button" data-toggle="collapse" data-target="#collapseB<?php echo $count2?>" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <div class="row ">
                                    <div class="col-7" style="text-align: left">{{ $guia -> titol }}</div>
                                    <div class="col-5 d-flex">
                                        <div class="col-6 d-flex">
                                            <p class="d-flex"><img src="{{ asset('img/icons/eye-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $guia -> visites }}</p>
                                        </div>
                                        <div class="col-6 d-flex flex-row pl-4">
                                            <p class="d-flex"><img src="{{ asset('img/icons/data-transfer-download-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $guia -> descargues }}</p>
                                        </div>
                                    </div>
                                </div>

                            </button>
                            <div class="collapse" style="background-color: #F0F0F0"; id="collapseB<?php echo $count2?>">
                                <div class="navbar-nav ml-auto">
                                    <div class="cosa">

                                        <div class="d-flex flex-row">
                                            @foreach($fotos as $foto)
                                                @if($guia->idFoto == $foto->id)
                                                    <div class="p-2 col-4"><img class="img-fluid border-dark" src="{{ asset($foto->ruta. $foto->nom) }}">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="p-2 col-8">

                                                <p class="badge badge-secondary">Fecha: {{$guia->created_at}}</p>

                                                <p><strong>Descripción: </strong>{{$guia->descripcio}}</p>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-row">
                                            <div class="p-2">
                                                <form action="/guies/{{$guia -> id}}" method="get">
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary">Ver mas</button>
                                                </form>
                                            </div>
                                            <div class="p-2 pr-5">
                                                @foreach($fitxer as $fitxers)
                                                    @if($fitxers->id == $guia->idFitxer)
                                                        @guest
                                                            <a href="{{ route('login') }}">
                                                                <button type="button" class="btn btn-success">Descargar</button>
                                                            </a>
                                                        @else
                                                            <a href="/downloadGuia/{{$guia -> id}}/{{$guia -> idFitxer}}">
                                                                <button type="button" class="btn btn-success">Descargar</button>
                                                            </a>
                                                        @endguest
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="p-2 pl-2">
                                                <form action="/editGuia/{{$guia -> id}}" method="get">
                                                    @csrf
                                                    <button type="submit" class="btn btn-warning">Editar</button>
                                                </form>
                                            </div>
                                            <div class="p-1 pt-2">
                                                <form action="/deleteGuia/{{$guia -> id}}" method="get">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger">Borrar</button>
                                                </form>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                    <?php ++$count2?>
            @endforeach
        @endforeach
    </div>
    </div>
    </div>
@endsection