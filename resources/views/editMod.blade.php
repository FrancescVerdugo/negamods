@extends('layouts.app')

@section('content')

    <div class="container" style="width: 70%">
        <form action="/updateMod/{{ $mod -> id }}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="Foto">Foto:</label>
                <input class="" type="file" id="foto" name="foto"  accept="image/*" value="{{$mod -> idFoto}}" required>
            </div>

            <div class="form-group">

                <label for="titol">Titulo:</label>
                <input type="text" class="form-control" id="titol" name="titol" maxlength="47" value="{{$mod -> titol}}" required/>

            </div>

            <div class="form-group">
                <label for="descripcio">Descripción:</label>
                <textarea class="form-control" id="descripcio" name="descripcio" maxlength="1000" required>{{$mod -> descripcio}} required</textarea>
            </div>

            <div class="">
                <input class="" type="file" id="archivo" name="archivo" accept=".zip" value="{{$mod -> idFitxer}} " required>
            </div>
            <br/>
            <div class="button">
                <button type="submit" class="btn btn-primary">Subir</button>
            </div>
        </form>
    </div>
@endsection