@extends('layouts.app')

@section('content')
        @if(isset($details))
            <h6>Resultados de la búsqueda de "<b>{{$entradaSearch}}</b>"</h6>
            <div class="row justify-content-between">
            @foreach($details as $videojocSearch)
                <div class="flip-div col-3">
                    <div class="flip-main">
                        <div class="front card border-dark">
                            @foreach($fotos as $foto)
                                @if($foto->id == $videojocSearch->idFoto)
                                    <img class="img-fluid border-dark caratula" alt="Portada de {{ $videojocSearch -> nom }}" src="{{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}">
                                @endif
                            @endforeach
                        </div>
                        <div class="back card  border-dark">
                            <div class="card-body text-center ">
                                <p class="card-title"><strong>{{ $videojocSearch -> nom }}</strong></p>
                                <ul class="list-inline">
                                    <li><p class="badge badge-secondary">{{ $videojocSearch -> data }}</p></li>
                                    <li class="d-flex" >
                                        <form action="/{{ $videojocSearch -> id }}/mods" method="get">
                                            @csrf
                                            <button class="btn btn-primary btn-sm" type="submit">MODS</button>
                                        </form>
                                        <form action="/{{ $videojocSearch -> id }}/guies" method="get">
                                            @csrf
                                            <button class="btn btn-secondary btn-sm" type="submit">GUIES</button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @elseif(isset($message))
            <div class="d-flex justify-content-center" style="padding-top:10%">
                <h1>{{$message}}</h1>
            </div>
        @else
            <div class="row justify-content-between" id="contenedor-carousel">
                <div class="container-fluid">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item py-5 active">
                            <div class="row carrousel-row">
                                @foreach($mods as $mod)
                                    <div class="flip-div col-3">
                                        <div >
                                            <div class="flip-main card  border-dark" style="background-image: url(@foreach($videojocs as $videojoc)
                                            @if($mod->idVideojoc == $videojoc->id)
                                            @foreach($fotos as $foto)
                                            @if($videojoc->idFoto == $foto->id)
                                            {{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}
                                            @endif
                                            @endforeach
                                            @endif
                                            @endforeach); background-size: 10em 12rem;">
                                                <div class="card-body text-center ">
                                                    <p style="background-color: rgba(255, 255, 255, 0.7)" class="card-title info-destacado"><strong>{{ $mod->titol }}</strong></p>
                                                    <ul class="list-inline">
                                                        <li><p class="badge badge-secondary">{{ $mod -> visites }} visites</p></li>
                                                        <li>
                                                            <form action="/mods/{{ $mod -> id }}" method="get">
                                                                @csrf
                                                                <button class="btn btn-primary btn-sm" type="submit">Anar-hi</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="carousel-caption text-white d-none d-sm-block">
                                <h4>Mods Destacados</h4>
                            </div>
                        </div>
                        <div class="carousel-item py-5">
                            <div class="row carrousel-row">
                                @foreach($guies as $guia)
                                    <div class="flip-div col-3">
                                        <div >
                                            <div class="flip-main card  border-dark" style="background-image: url(@foreach($videojocs as $videojoc)
                                            @if($guia->idVideojoc == $videojoc->id)
                                            @foreach($fotos as $foto)
                                            @if($videojoc->idFoto == $foto->id)
                                            {{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}
                                            @endif
                                            @endforeach
                                            @endif
                                            @endforeach); background-size: 10em 12rem;">
                                                <div class="card-body text-center ">
                                                    <p style="background-color: rgba(255, 255, 255, 0.7)" class="card-title info-destacado"><strong>{{ $guia->titol }}</strong></p>
                                                    <ul class="list-inline">
                                                        <li><p class="badge badge-secondary">{{ $guia -> visites }} visites</p></li>
                                                        <li>
                                                            <form action="/guies/{{ $guia -> id }}" method="get">
                                                                @csrf
                                                                <button class="btn btn-primary btn-sm" type="submit">Anar-hi</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="carousel-caption text-white d-none d-sm-block">
                                <h4>Guias Destacadas</h4>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev left" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next right" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                </div>
            </div>
            <div class="row justify-content-between" id="contenidor-videojocs">
            @foreach($videojocs as $videojoc)
                <div class="flip-div col-3">
                    <div class="flip-main">
                        <div class="front card border-dark">
                            @foreach($fotos as $foto)
                                @if($foto->id == $videojoc->idFoto)
                                        <img class="img-fluid border-dark caratula" alt="Portada de {{ $videojoc -> nom }}" src="{{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}">
                                @endif
                            @endforeach
                        </div>
                        <div class="back card  border-dark">
                                <div class="card-body text-center ">
                                    <p class="card-title"><strong>{{ $videojoc -> nom }}</strong></p>
                                    <ul class="list-inline">
                                        <li><p class="badge badge-secondary">{{ $videojoc -> data }}</p></li>
                                        <li class="d-flex" >
                                            <form action="/{{ $videojoc -> id }}/mods" method="get">
                                                @csrf
                                                <button class="btn btn-primary btn-sm" type="submit">MODS</button>
                                            </form>
                                            <form action="/{{ $videojoc -> id }}/guies" method="get">
                                                @csrf
                                                <button class="btn btn-secondary btn-sm" type="submit">GUIES</button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
@endsection

