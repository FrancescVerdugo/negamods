@extends('layouts.app')

@section('content')

    <?php $count=0;?>

    <script>
        $(".ignore-click").click(function(){
            return false;
        })
    </script>

    <div class="container">
        <div class="row container p-0">

            <div class="col-3 p-0 pl-2 text-center">
                @foreach($fotos as $foto)
                    @if($videojoc->idFoto == $foto->id)
                            <img class="border-dark img-fluid" style="width: 80%" src="{{ asset($foto->ruta. $foto->nom. '.'.$foto->extensio) }}">
                    @endif
                @endforeach
            </div>

            <div class="col-9 p-0 ">
                <div class="row">
                    <div class="col-10">
                        <h2>{{$videojoc->nom}} - Guies</h2>
                    </div>
                    <div class="container text-right col-2  ">
                        <form action="/{{ $videojoc-> id }}/mods/" method="get">
                        @csrf
                        <button type="submit" class="btn btn-danger">Mods</button>
                        </form>
                    </div>
                </div>
                <div>
                    <p class="badge badge-secondary">{{ $videojoc -> data }}</p>
                </div>
                <div class="p-2 border" style="background-color: #EEEFF3">
                    <p>
                        <strong>Descripción:</strong><br/>
                        {{$videojoc->descripcio}}
                    </p>
                </div>
            </div>


        </div>
        <hr/>
        @foreach($guies as $guia)
            <div class="flex-row">
                <div class="p-2">

                    <button class="navbar-toggler pl-3 pr-3 pt-1 pb-1 bg-info text-white" style="width: 100%" type="button" data-toggle="collapse" data-target="#collapse<?php echo $count?>" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

                        <div class="row ">
                            <div class="col-9" style="text-align: left">{{ $guia -> titol }}</div>
                            <div class="col-3 d-flex">
                                <div class="col-6 d-flex">
                                    <p class="d-flex"><img src="{{ asset('img/icons/eye-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $guia -> visites }}</p>
                                </div>
                                <div class="col-6 d-flex flex-row pl-4">
                                    <p class="d-flex"><img src="{{ asset('img/icons/data-transfer-download-8x.png') }}" alt="Smiley face" height="15" width="15"></p><p class="d-flex pl-1">{{ $guia -> descargues }}</p>
                                </div>
                            </div>
                        </div>

                    </button>

                    <div class="collapse" style="background-color: #F0F0F0;"id="collapse<?php echo $count?>">
                        <div class="navbar-nav ml-auto">
                            <div class="cosa">

                                <div class="d-flex flex-row">
                                    @foreach($fotos as $foto)
                                        @if($guia->idFoto == $foto->id)
                                            <div class="p-2 col-3"><img class="img-fluid border-dark" src="{{ asset($foto->ruta. $foto->nom) }}">
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="p-2 col-8">

                                        <p class="badge badge-secondary">Fecha: {{$guia->created_at}}</p>
                                        <p class="badge badge-secondary">Autor:

                                            @foreach($users as $user)
                                                @if($user->id == $guia->idUser)
                                                    {{$user->name}}
                                                @endif
                                            @endforeach</p>

                                        <p><strong>Descripción: </strong>{{$guia->descripcio}}</p>
                                    </div>


                                        @if(count($preferits))
                                            <?php $aux=0; ?>
                                            @foreach($preferits as $preferit)
                                                @if($preferit->idGuia == $guia->id)
                                                    @if(Auth::id() == $preferit -> idUser)
                                                        <div class="text-right pl-4">
                                                            <div class="text-right col-1"><a href="/addPreferitGuia/{{$guia -> id}}/0" class="ignore-click"><img class="" src="{{ asset('img/icons/yel.svg') }}" style="width: 17px; height: 17px"></a></div>
                                                        </div>
                                                        <?php $aux = 'jaEsta' ?>
                                                    @endif
                                                @endif
                                                <?php ++$aux ?>
                                            @endforeach
                                            @if($aux == count($preferits))
                                                <div class="text-right pl-4">
                                                    <div class="text-right col-1"><a href="/addPreferitGuia/{{$guia -> id}}/1" class="ignore-click"><img class="" src="{{ asset('img/icons/blk.png') }}" style="width: 17px; height: 17px"></a></div>
                                                </div>
                                            @endif
                                        @else
                                            <div class="text-right pl-4">
                                                <div class="text-right col-1"><a href="/addPreferitGuia/{{$guia -> id}}/1" class="ignore-click"><img class="" src="{{ asset('img/icons/blk.png') }}" style="width: 17px; height: 17px"></a></div>
                                            </div>
                                        @endif
                                </div>


                                <div class="d-flex flex-row">
                                    <div class="p-2">
                                        <form action="/guies/{{$guia -> id}}" method="get">
                                            @csrf
                                            <button type="submit" class="btn btn-primary">Ver mas</button>
                                        </form>
                                    </div>
                                    <div class="p-2">
                                        @foreach($fitxers as $fitxer)
                                            @if($fitxer->id == $guia->idFitxer)
                                                @guest
                                                    <a href="{{ route('login') }}">
                                                        <button type="button" class="btn btn-success">Descargar</button>
                                                    </a>
                                                @else
                                                    <a href="/downloadGuia/{{$guia -> id}}/{{$guia -> idFitxer}}">
                                                        <button type="button" class="btn btn-success">Descargar</button>
                                                    </a>
                                                @endguest
                                            @endif
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php ++$count?>
        @endforeach

    </div>
@endsection