@extends('layouts.app')

@section('content')

    <div class="container" style="width: 70%">
        <form action="/guardarMod" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="videojuego">Videojuego</label>
                <select class="form-control" style="width:50% " id="idVideojoc" name="idVideojoc">
                    @foreach($videojocs as $videojoc)
                        <option value="{{$videojoc->id}}">{{$videojoc->nom}}</option>
                    @endforeach

                </select>
            </div>
            <div class="form-group">
                <label for="Foto">Foto:</label>
                <input class="" type="file" id="foto" name="foto"  accept="image/*" required>
            </div>

            <div class="form-group">

                <label for="titol">Titulo:</label>
                <input type="text" class="form-control" id="titol" name="titol" maxlength="47" required/>

            </div>

            <div class="form-group">
                <label for="descripcio">Descripción:</label>
                <textarea class="form-control" id="descripcio" name="descripcio" maxlength="1000" required></textarea>
            </div>

            <div class="">
                <p>(El archivo debe ser .zip)</p>
                <input class="" type="file" id="archivo" name="archivo" accept=".zip" required>
            </div>
            <br/>
            <div class="button">
                <button type="submit" class="btn btn-primary">Subir</button>
            </div>
        </form>
    </div>
@endsection