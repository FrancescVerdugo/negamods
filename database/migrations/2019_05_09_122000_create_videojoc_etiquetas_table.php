<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideojocEtiquetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videojoc_etiquetas', function (Blueprint $table) {
            $table->unsignedBigInteger('idVideojoc');
            $table->unsignedBigInteger('idEtiqueta');
            $table->foreign('idVideojoc')->references('id')->on('videojocs');
            $table->foreign('idEtiqueta')->references('id')->on('etiquetas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videojoc_etiquetas');
    }
}
