<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titol')->unique();
            $table->string('descripcio');
            $table->integer('like');
            $table->integer('dislike');
            $table->unsignedBigInteger('idFitxer');
            $table->unsignedBigInteger('idVideojoc');
            $table->unsignedBigInteger('idComentari');
            $table->unsignedBigInteger('idUser');
            $table->foreign('idFitxer')->references('id')->on('fitxers');
            $table->foreign('idVideojoc')->references('id')->on('videojocs');
            $table->foreign('idComentari')->references('id')->on('comentaris');
            $table->foreign('idUser')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mods');
    }
}
