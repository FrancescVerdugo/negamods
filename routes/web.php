<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'Init@index')->name('home');
Route::get('/{idVideojoc}/mods', 'Mods@mods');
Route::get('/{idVideojoc}/guies', 'Guies@guies');
Route::get('/favoritos', 'Init@preferits')->middleware('auth');
Route::get('/mods/{idMod}','Mods@modEspecific');
Route::get('/guies/{idGuia}','Guies@guiaEspecifica');
Route::get('/crearGuia', 'Guies@crearGuia')->middleware('auth');
Route::get('/crearMod', 'Mods@crearMod')->middleware('auth');
Route::post('/guardarMod', 'Mods@guardarMod')->middleware('auth');
Route::post('/guardarGuia', 'Guies@guardarGuia')->middleware('auth');
Route::get('/miContenido', 'Init@miContenido')->middleware('auth');

Route::post('/guardarComentari', 'Init@guardarComentari')->middleware('auth');

Route::get('/addPreferitMod/{idMod}/{esPreferit}', 'Mods@addPreferitMod')->middleware('auth');
Route::get('/addPreferitGuia/{idGuia}/{esPreferit}', 'Guies@addPreferitGuia')->middleware('auth');

Route::get('/deleteMod/{idMod}', 'Mods@deleteMod')->middleware('auth');
Route::get('/deleteGuia/{idGuia}', 'Guies@deleteGuia')->middleware('auth');

Route::get('/deleteComentariMod/{idComentari}/{idMod}', 'Init@deleteComentariMod')->middleware('auth');
Route::get('/deleteComentariGuia/{idComentari}/{idGuia}', 'Init@deleteComentariGuia')->middleware('auth');

Route::get('/editMod/{idMod}', 'Mods@editMod')->middleware('auth');
Route::post('/updateMod/{idMod}', 'Mods@updateMod')->middleware('auth');

Route::get('/editGuia/{idGuia}', 'Guies@editGuia')->middleware('auth');
Route::post('/updateGuia/{idGuia}', 'Guies@updateGuia')->middleware('auth');

Route::get('/downloadMod/{idMod}/{idFitxer}', 'Mods@getFile')->middleware('auth');
Route::get('/downloadGuia/{idGuia}/{idFitxer}', 'Guies@getFile')->middleware('auth');

Route::any('/buscar','Init@modeBusca');