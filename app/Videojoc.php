<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videojoc extends Model
{

    public function etiqueta()
    {
        return $this->belongsToMany('App\Etiqueta')->using('App\VideojocEtiqueta');
    }
    public function mods()
    {
        return $this->hasMany('App\Mod');
    }
}
