<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mod extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User')->using('App\mod_user');
    }

    public function videojoc()
    {
        return $this->belongsTo('App\Videojoc');
    }
}
