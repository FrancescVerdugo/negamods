<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guia extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\Users')->using('App\PreferitsGuia');
    }
}
