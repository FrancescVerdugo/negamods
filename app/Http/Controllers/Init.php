<?php


namespace App\Http\Controllers;


use App\Comentari;
use App\Fitxer;
use App\Fotos;
use App\Guia;
use App\Mod;
use App\mod_user;
use App\PreferitsGuia;
use App\User;
use App\Videojoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Init extends Controller
{
    public function index()
    {
        $videojocs = Videojoc::all();
        $mods = DB::table('mods')->orderBy('visites', 'desc')->take(4)->get();
        $guies = DB::table('guias')->orderBy('visites', 'desc')->take(4)->get();
        $fotos = Fotos::all();
        return view('index', ['videojocs'=>$videojocs, 'mods'=>$mods, 'guies'=>$guies, 'fotos'=>$fotos, 'gameSearch'=>$videojocs]);
    }

    public function preferits()
    {
        $id = Auth::id();
        $videojoc = Videojoc::all();
        $guies = Guia::all();
        $fitxer = Fitxer::all();
        $fotos = Fotos::all();
        $mods = Mod::all();
        $preferitsGuies = DB::table('preferits_guias')->get();
        $preferitsMods = DB::table('mod_users')->get();
        $modsUser[] = new mod_user();
        $guiesUser[] = new PreferitsGuia();
        foreach ($mods as $mod){
            foreach ($preferitsMods as $preferitsMod){
                if($id == $preferitsMod -> user_id){
                    if($preferitsMod->mod_id == $mod->id){
                        $modsUser[] = Mod::find($mod->id);
                    }
                }
            }

        }

        foreach ($guies as $guia){
            foreach ($preferitsGuies as $preferitsGuia){
                if($id == $preferitsGuia -> idUser){
                    if($preferitsGuia->idGuia == $guia->id){
                        $guiesUser[] = Guia::find($guia->id);
                    }
                }
            }

        }

        return view('preferits', ['id'=>$id, 'mods'=>$modsUser, 'videojoc'=>$videojoc, 'guies'=> $guiesUser, 'fitxer'=>$fitxer, 'gameSearch'=>$videojoc, 'fotos'=>$fotos, 'preferitsGuies'=>$preferitsGuies, 'preferitsMods'=>$preferitsMods]);
    }

    public function  miContenido(){
        $idUser = Auth::id();
        $mods = DB::table('mods')->where('idUser','=', $idUser)->get();
        $guies = DB::table('guias')->where('idUser','=', $idUser)->get();
        $videojoc = Videojoc::all();
        $fitxer = Fitxer::all();
        $fotos = Fotos::all();
        return view('miContenido', ['mods'=>$mods, 'guies'=>$guies, 'videojoc'=> $videojoc, 'fitxer'=>$fitxer, 'gameSearch'=>$videojoc, 'fotos'=>$fotos]);
    }

    public function guardarComentari(Request $request) {
        $idUser = Auth::id();
        if($request -> input('esMod')=='esMod'){
            $comentaris = new Comentari();
            $comentaris -> contingut = $request -> input('comentariNou');
            $comentaris -> idMod = $request -> input('idMod');
            $comentaris -> idUser = $idUser;
            $comentaris -> save();
            $idMod = $request -> input('idMod');
            return redirect('/mods/'.$idMod);
        }else{
            $comentaris = new Comentari();
            $comentaris -> contingut = $request -> input('comentariNou');
            $comentaris -> idGuia = $request ->input('idGuia');
            $comentaris -> idUser = $idUser;
            $comentaris -> save();
            $idGuia = $request -> input('idGuia');
            return redirect('/guies/'.$idGuia);
        }
    }

    public function deleteComentariMod($idComentari, $idMod){
        try{
            DB::table('comentaris')->where('id','=', $idComentari)->delete();
        }catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect('/mods/'.$idMod);
    }

    public function deleteComentariGuia($idComentari, $idGuia){
        try{
            DB::table('comentaris')->where('id','=', $idComentari)->delete();
        }catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect('/guies/'.$idGuia);
    }

    public function modeBusca(){
        $fotos = Fotos::all();
        $entradaSearch = Input::get ( 'entradaSearch' );
        $gameSearch = Videojoc::all();
        $videojocs = DB::table('videojocs')->where('nom','LIKE','%'.$entradaSearch.'%')->get();
        if(count($videojocs) > 1){
            return view('index',['videojocs'=>$videojocs, 'fotos'=>$fotos, 'gameSearch'=>$gameSearch,'entradaSearch'=>$entradaSearch])->withDetails($videojocs)->withQuery ( $entradaSearch );
        }else if(count($videojocs) == 1){
            $idVideojoc = DB::table('videojocs')->where('nom','=',$entradaSearch)->first();
            if($idVideojoc!=null){
                return redirect('/'.$idVideojoc->id.'/mods');
            }else{
                return view('index',['videojocs'=>$videojocs, 'fotos'=>$fotos, 'gameSearch'=>$gameSearch,'entradaSearch'=>$entradaSearch])->withDetails($videojocs)->withQuery ( $entradaSearch );
            }
        }else{
            return view ('index',['videojocs'=>$videojocs,  'fotos'=>$fotos, 'gameSearch'=>$gameSearch])->withMessage('No hemos encontrado ningún resultado de "'.$entradaSearch.'"');
        }
    }

}