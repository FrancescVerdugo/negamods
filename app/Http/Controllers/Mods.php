<?php


namespace App\Http\Controllers;


use App\Fitxer;
use App\Fotos;
use App\Mod;
use App\mod_user;
use App\User;
use App\Videojoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Mods extends Controller
{

    public function mods($idVideojoc)
    {
        $users = User::all();
        $mods = DB::table('mods')->where('idVideojoc', '=',$idVideojoc)->get();
        $fitxers = Fitxer::all();
        $videojoc = Videojoc::find($idVideojoc);
        $gameSearch = Videojoc::all();
        $fotos = Fotos::all();
        $preferits = DB::table('mod_users')->get();
        return view('mods', ['mods'=>$mods, 'users'=>$users, 'fitxers'=>$fitxers, 'videojoc'=>$videojoc, 'gameSearch'=>$gameSearch, 'fotos'=>$fotos, 'preferits'=>$preferits]);
    }

    public function modEspecific($idMod)
    {
        DB::transaction(function () use ($idMod) {
            DB::table('mods')->where('id', $idMod)->increment("visites" ,1);
        });
        $mods = DB::table('mods')->where('id', '=',$idMod)->first();
        $comentaris = DB::table('comentaris')->where('idMod','=', $idMod)->get();
        $user = User::all();
        $fitxer = DB::table('fitxers')->where('id','=',$mods->idFitxer)->first();
        $foto = Fotos::find($mods->idFoto);
        $videojoc = Videojoc::all();
        return view('modEspecific', ['mods'=>$mods, 'user'=>$user, 'comentaris'=>$comentaris, 'fitxer'=>$fitxer, 'gameSearch'=>$videojoc, 'foto'=>$foto]);
    }

    public function crearMod(){
        $videojocs = Videojoc::all();
        return view('crearMod',['videojocs'=>$videojocs, 'gameSearch'=>$videojocs]);
    }

    public function guardarMod(Request $request){
        $date = new \DateTime();
        $result = $date->format('Y-m-dH:i:s');
        $idUser = Auth::id();

        //Guardar fitxer
        $archivo = $request -> file('archivo');
        $archivo->move(base_path('/public/archivos/'), $result. $idUser. $archivo->getClientOriginalName());
        $file = new Fitxer();
        $file -> nom = $result. $idUser. $archivo->getClientOriginalName();
        $file -> ruta = 'archivos/';
        $file -> extensio = $archivo->clientExtension();
        $file ->save();

        //Guargar foto
        $fotoReq = $request -> file('foto');
        $fotoReq->move(base_path('/public/imgMods/'), $result. $idUser. $fotoReq->getClientOriginalName());
        $foto = new Fotos();
        $foto -> nom = $result. $idUser. $fotoReq->getClientOriginalName();
        $foto -> ruta = 'imgMods/';
        $foto -> extensio = $fotoReq->clientExtension();
        $foto ->save();

        $fileId = DB::table('fitxers')->where('nom','=', $file -> nom)->value('id');
        $fotoId = DB::table('fotos')->where('nom', '=', $foto -> nom)->value('id');

        $mod = new Mod();
        $mod -> titol = $request->input('titol');
        $mod -> descripcio = $request->input('descripcio');
        $mod -> idFitxer = $fileId;
        $mod -> idFoto = $fotoId;
        $mod -> idVideojoc = $request->input('idVideojoc');
        $mod -> idUser = $idUser;
        $mod -> save();

        return redirect('/miContenido');
    }

    public function editMod($idMod){
        $gameSearch = Videojoc::all();
        $mod = Mod::find($idMod);
        return view('editMod', ['mod'=>$mod, 'gameSearch'=>$gameSearch]);
    }

    public function updateMod(Request $request, $idMod){
        $mod = Mod::find($idMod);
        $date = new \DateTime();
        $result = $date->format('Y-m-d H:i:s');
        $idUser = Auth::id();
        try{
            DB::table('mods')->where('id', '=',$idMod)->update(['titol' => $request -> titol, 'descripcio' => $request -> descripcio]);
            if($request->archivo != null){
                $fitxerAntic = Fitxer::find($mod -> idFitxer);
                $archivo = $request -> file('archivo');
                rename($fitxerAntic->ruta. $fitxerAntic->nom, $fitxerAntic->ruta. $result. $idUser. $archivo->getClientOriginalName());
                DB::table('fitxers')->where('id','=', $fitxerAntic -> id)->update(['nom' => $result. $idUser. $archivo->getClientOriginalName()]);
            }
            if($request->foto != null){
                $fotoAntiga = Fotos::find($mod -> idFoto);
                $foto = $request -> file('foto');
                rename($fotoAntiga->ruta. $fotoAntiga->nom, $fotoAntiga->ruta. $result. $idUser. $foto->getClientOriginalName());
                DB::table('fotos')->where('id','=', $fotoAntiga -> id)->update(['nom' => $result. $idUser. $foto->getClientOriginalName()]);
            }

        }catch(\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        return redirect('/miContenido');
    }

    public function deleteMod($idMod){
        try{
            $mod = Mod::find($idMod);
            $idFitxr = $mod->idFitxer;
            DB::table('mods')->where('id','=', $idMod)->delete();
            DB::table('fitxers')->where('id','=', $idFitxr)->delete();
        }catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect('/miContenido');
    }

    public function getFile($idMod, $idFitxer) {
        $fitxer = Fitxer::find($idFitxer);
        DB::transaction(function () use ($idMod) {
            DB::table('mods')->where('id', $idMod)->increment("descargues" ,1);
        });
            return response()->download(public_path($fitxer->ruta. $fitxer->nom));
    }

    public function addPreferitMod($idMod, $esPreferit){
        $idUser = Auth::id();
        $mod = Mod::find($idMod);
        if($esPreferit == 1){
            $preferitMod  = new mod_user();
            $preferitMod -> user_id = $idUser;
            $preferitMod -> mod_id = $mod->id;
            $preferitMod -> save();
        }else{
            DB::table('mod_users')->where('mod_id','=',$idMod)->where('user_id','=',$idUser)->delete();
        }
        return back();
    }

}