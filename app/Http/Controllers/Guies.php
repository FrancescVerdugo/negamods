<?php


namespace App\Http\Controllers;


use App\Fitxer;
use App\Fotos;
use App\Guia;
use App\PreferitsGuia;
use App\User;
use App\Videojoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Guies extends Controller
{
    public function guies($idVideojoc)
    {
        $users = User::all();
        $guies = DB::table('guias')->where('idVideojoc','=',$idVideojoc)->get();
        $fitxers = Fitxer::all();
        $videojoc = Videojoc::find($idVideojoc);
        $gameSearch = Videojoc::all();
        $fotos = Fotos::all();
        $preferits = DB::table('preferits_guias')->get();
        return view('guies', ['guies'=>$guies,'users'=>$users, 'fitxers'=>$fitxers, 'videojoc'=>$videojoc, 'gameSearch'=>$gameSearch, 'fotos'=>$fotos, 'preferits'=>$preferits]);
    }

    public function guiaEspecifica($idGuia)
    {
        DB::transaction(function () use ($idGuia) {
            DB::table('guias')->where('id', $idGuia)->increment("visites" ,1);
        });

        $guies = DB::table('guias')->where('id', '=',$idGuia)->first();
        $comentaris = DB::table('comentaris')->where('idGuia','=', $idGuia)->get();
        $user = User::all();
        $fitxer = DB::table('fitxers')->where('id','=',$guies->idFitxer)->first();
        $gameSearch = Videojoc::all();
        $foto = Fotos::find($guies->idFoto);
        return view('guiaEspecifica', ['guies'=>$guies, 'user'=>$user, 'comentaris'=>$comentaris, 'fitxer'=>$fitxer, 'gameSearch'=>$gameSearch, 'foto'=>$foto]);
    }

    public function crearGuia(){
        $videojocs = Videojoc::all();
        return view('crearGuia',['videojocs'=>$videojocs, 'gameSearch'=>$videojocs]);
    }

    public function guardarGuia(Request $request){
        $date = new \DateTime();
        $result = $date->format('Y-m-d H:i:s');
        $idUser = Auth::id();

        //Guardar fitxer
        $archivo = $request -> file('archivo');
        $archivo->move(base_path('/public/archivos/'), $result. $idUser. $archivo->getClientOriginalName());
        $file = new Fitxer();
        $file -> nom = $result. $idUser. $archivo->getClientOriginalName();
        $file -> ruta = 'archivos/';
        $file -> extensio = $archivo->clientExtension();
        $file ->save();

        //Guargar foto
        $fotoReq = $request -> file('foto');
        $fotoReq->move(base_path('/public/imgGuia/'), $result. $idUser. $fotoReq->getClientOriginalName());
        $foto = new Fotos();
        $foto -> nom = $result. $idUser. $fotoReq->getClientOriginalName();
        $foto -> ruta = 'imgGuia/';
        $foto -> extensio = $fotoReq->clientExtension();
        $foto ->save();

        $fileId= DB::table('fitxers')->where('nom','=', $file -> nom)->value('id');
        $fotoId = DB::table('fotos')->where('nom', '=', $foto -> nom)->value('id');

        $guia = new Guia();
        $guia -> titol = $request->input('titol');
        $guia -> descripcio = $request->input('descripcio');
        $guia -> idVideojoc = $request->input('idVideojoc');
        $guia -> idFoto = $fotoId;
        $guia -> idFitxer = $fileId;
        $guia -> idUser = $idUser;
        $guia -> save();

        return redirect('/miContenido');
    }

    public function editGuia($idGuia){
        $gameSearch = Videojoc::all();
        $guia = DB::table('guias')->where('id','=',$idGuia)->get();
        return view('editGuia', ['guia'=>$guia, 'gameSearch'=>$gameSearch]);
    }

    public function updateGuia(Request $request, $idGuia){
        $guia = Guia::find($idGuia);
        $date = new \DateTime();
        $result = $date->format('Y-m-d H:i:s');
        $idUser = Auth::id();
        try{
            DB::table('guias')->where('id', '=',$idGuia)->update(['titol' => $request -> titol, 'descripcio' => $request -> descripcio]);
            if($request->archivo != null){
                $fitxerAntic = Fitxer::find($guia -> idFitxer);
                $archivo = $request -> file('archivo');
                rename($fitxerAntic->ruta. $fitxerAntic->nom, $fitxerAntic->ruta. $result. $idUser. $archivo->getClientOriginalName());
                DB::table('fitxers')->where('id','=', $fitxerAntic -> id)->update(['nom' => $result. $idUser. $archivo->getClientOriginalName()]);
            }
            if($request->foto != null){
                $fotoAntiga = Fotos::find($guia -> idFoto);
                $foto = $request -> file('foto');
                rename($fotoAntiga->ruta. $fotoAntiga->nom, $fotoAntiga->ruta. $result. $idUser. $foto->getClientOriginalName());
                DB::table('fotos')->where('id','=', $fotoAntiga -> id)->update(['nom' => $result. $idUser. $foto->getClientOriginalName()]);
            }
        }catch(\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

        return redirect('/miContenido');
    }

    public function deleteGuia($idGuia){
        try{
            $guia = Guia::find($idGuia);
            $idFitxr = $guia->idFitxer;
            DB::table('guias')->where('id','=', $idGuia)->delete();
            DB::table('fitxers')->where('id','=', $idFitxr)->delete();
        }catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect('/miContenido');
    }

    public function getFile($idGuia, $idFitxer) {
        $fitxer = Fitxer::find($idFitxer);
        DB::transaction(function () use ($idGuia) {
            DB::table('guias')->where('id', $idGuia)->increment("descargues" ,1);
        });
        return response()->download(public_path($fitxer->ruta. $fitxer->nom));
    }

    public function addPreferitGuia($idGuia, $esPreferit){
        $idUser = Auth::id();
        $guia = Guia::find($idGuia);
        if($esPreferit == 1){
            $preferitGuia  = new PreferitsGuia();
            $preferitGuia -> idUser = $idUser;
            $preferitGuia -> idGuia = $guia->id;
            $preferitGuia -> save();
        }else{
            DB::table('preferits_guias')->where('idGuia','=',$guia)->where('idUser','=',$idUser)->delete();
        }
        return back();
    }
}